#!/usr/bin/env python3

import asyncio
import time
import os
import cozmo
from PIL import Image

def suche_Wuerfel(robot):



    robot.say_text("Hallo, ich heisse Cozmo und suche einen Wuerfel").wait_for_completed()

    # Schaut sich um, um den Wuerfel zu finden.
    umschauen = robot.start_behavior(
        cozmo.behavior.BehaviorTypes.LookAroundInPlace)

    wuerfel = None
    gedrueckt = None

    try:
        # Versuche Wuerfel zu finden. Wenn nach 60 Sekunden keiner gefunden wird, wird abgebrochen.
        wuerfel = robot.world.wait_for_observed_light_cube(timeout=60)

        # setze die Farbe des gefunden Wuerfels auf gruen
        wuerfel.set_lights(cozmo.lights.green_light)
        print("Wuerfel gefunden", wuerfel)

        # Umherschauen beenden.
        umschauen.stop()

        robot.say_text("Druecke auf den leuchtenden Wuerfel")

        # warte darauf das der Wuerfel gedrueckt wird
        gedrueckt = wuerfel.wait_for_tap(timeout=60)

        if gedrueckt: # wenn Wuerfel gedrueckt wird, dann
            wuerfel.set_lights_off()
            setzeBild_aufScreen(robot)

    except asyncio.TimeoutError:
        # Wird nur ausgefuehrt wenn die Zeit vorbei ist und dabei kein Wuerfel gefunden wurde.
        umschauen.stop()
        print("Wuerfel nicht gefunden :-(")



def setzeBild_aufScreen(robot):

    # navigiere zu Ordner (Pfad) in dem die Bilder liegen
    momentaner_ordner = os.path.dirname(os.path.realpath(__file__))

    # Bilder mit ihrem Pfad
    hello_png = os.path.join(momentaner_ordner, "hello_world.png")
    sdk_png = os.path.join(momentaner_ordner, "cozmosdk.png")
    # herz_png = os.path.join(momentaner_ordner, "herz.png")


    # Lade Bilder
    bild_einstellungen = [(hello_png, Image.BICUBIC),
                      (sdk_png, Image.NEAREST) ] #, (herz_png, Image.NEAREST)

    screen_bilder = []

    for bild_name, resampling_mode in bild_einstellungen:
        bild = Image.open(bild_name)

        # passe Bild an, sodass es auf den Bildschirm von Cozmo passt
        angepasstes_Bild = bild.resize(cozmo.oled_face.dimensions(), resampling_mode)

        # Konvertiere Bild in das Format des Screens von Cozmo
        screen_bild = cozmo.oled_face.convert_image_to_screen_data(angepasstes_Bild, invert_image=True)
        screen_bilder.append(screen_bild)

    dauer_in_sekunden = 2.0

    # zeige jedes Bild jeweils 2 Sekunden an
    for bild in screen_bilder:
        robot.display_oled_face_image(bild, dauer_in_sekunden * 1000.0)
        time.sleep(dauer_in_sekunden)

    time.sleep(1)

def folge_Gesicht(robot: cozmo.robot.Robot):

    robot.say_text("Nun suche ich ein Gesicht, dem ich folgen kann").wait_for_completed()

    # Bewege Cozmos Kopf nach oben
    robot.set_head_angle(cozmo.robot.MAX_HEAD_ANGLE).wait_for_completed()
    gesicht = None

    while True:
        aktion = None
        if gesicht:

            # Richtung Gesicht bewegen
            aktion = robot.turn_towards_face(gesicht)

        if not (gesicht and gesicht.is_visible):
            # Finde Gesicht
            try:
                gesicht = robot.world.wait_for_observed_face(timeout=30)

            except asyncio.TimeoutError:
                print("Kein Gesicht gefunden - exiting!")

        if aktion:
            # Warte bis Aktion beendet ist
            aktion.wait_for_completed()

        time.sleep(.1)



def main(robot):

    suche_Wuerfel(robot)
    folge_Gesicht(robot)


# starte Programm, oeffne Kamerasicht des Roboters
cozmo.run_program(main, use_viewer=True, force_viewer_on_top=True)
