import cozmo
from cozmo.util import degrees, distance_mm, speed_mmps
import time

# https://www.youtube.com/watch?v=SBtRgnfVNiw

def cozmo_program(robot: cozmo.robot.Robot):

    #robot.play_audio(cozmo.audio.AudioEvents.MusicFunLoop)

    robot.drive_straight(distance_mm(150), speed_mmps(50)).wait_for_completed() # 150 mm vorwaerts fahren mit 50 mm pro Sekunde
    robot.turn_in_place(degrees(90)).wait_for_completed() # dreht sich um 90 Grad nach links
    time.sleep(2) # 2 Sekunden Pause um den Wuerfel auf Cozmos Schaufel zu legen
    robot.move_lift(2) # Hebt Schaufe mit 2 Radianden per Sekunde (Wuerfel hochheben)
    time.sleep(2) # 2 Sekunden Pause
    robot.turn_in_place(degrees(180)).wait_for_completed()
    robot.drive_straight(distance_mm(150), speed_mmps(50)).wait_for_completed()
    robot.move_lift(-2) # Senke Schaufel mit 2 Radianden per Sekunde (Wuerfel abstellen)
    time.sleep(2)
    robot.drive_straight(distance_mm(-150), speed_mmps(50)).wait_for_completed() # Minus bewirkt entgegengesetzte Richtung (nach hinten fahren)

cozmo.run_program(cozmo_program)