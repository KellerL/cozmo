#!/usr/bin/env python3

import cozmo
import asyncio
from cozmo.util import degrees, distance_mm, speed_mmps

def sing_song(robot: cozmo.robot.Robot):
    # Legt eine Liste an, die die Noten fuer das Lied enthalten
    notes = [
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter)
    ]

    # Spielt das Lied so oft wie die Zahl die bei loop_count steht
    robot.play_song(notes, loop_count=1).wait_for_completed()

async def find_cube_and_take_action(robot: cozmo.robot.Robot, color_of_cube, task_id):
    """ Looks around and tries to find Cozmo's cubes.
    If task_id equals 0, Cozmo will dock to the found cube. Any other number will make Cozmo to approach the found cube.
    """
    robot.set_head_angle(degrees(-5.0)).wait_for_completed()
    look_around = robot.start_behavior(cozmo.behavior.BehaviorTypes.LookAroundInPlace)

    cube = None

    try:
        cube = robot.world.wait_for_observed_light_cube(timeout=60)
        print("Found cube", cube)

    except asyncio.TimeoutError:
        print("Didn't find a cube :-(")

    finally:
        # whether we find it or not, we want to stop the behavior
        look_around.stop()

    if cube:
        cube.set_lights(color_of_cube)
        if task_id == 0:
            action = robot.dock_with_cube(cube, approach_angle=cozmo.util.degrees(90), num_retries=2)
            amount = 2
        else:
            action = robot.go_to_object(cube, distance_mm(40.0))
            amount = - 2
        action.wait_for_completed()
        print("result:", action.result)

    else:
        robot.play_anim_trigger(cozmo.anim.Triggers.MajorFail)
        print("Failed")

    robot.move_lift(amount)

def main(robot):
    # Cozmo sagt den Text in Anfuehrungszeichen
    #robot.move_lift(-2)
    robot.say_text("Hallo, ich heisse Cozmo und staple nun Wuerfel.").wait_for_completed()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(find_cube_and_take_action(robot, cozmo.lights.blue_light, 0))
    loop.run_until_complete(find_cube_and_take_action(robot, cozmo.lights.green_light, 1))
    loop.close()

    robot.drive_straight(distance_mm(-100), speed_mmps(
    50)).wait_for_completed()  # Minus bewirkt entgegengesetzte Richtung (nach hinten fahren)

    sing_song(robot)


cozmo.run_program(main)