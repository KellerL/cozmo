import cozmo
import asyncio
import time
from cozmo.util import degrees, distance_mm, speed_mmps

def go_to_object_test(robot: cozmo.robot.Robot):

    # Move lift down and tilt the head up
    robot.move_lift(-3)
    robot.set_head_angle(degrees(-5)).wait_for_completed()

    # look around and try to find a cube
    look_around = robot.start_behavior(cozmo.behavior.BehaviorTypes.LookAroundInPlace)

    cube = None

    try:
        cube = robot.world.wait_for_observed_light_cube(timeout=30)
        print("Found cube", cube)

    except asyncio.TimeoutError:
        print("Didn't find a cube :-(")

    finally:
        # whether we find it or not, we want to stop the behavior
        look_around.stop()

    if cube:

        cube.set_lights(cozmo.lights.green_light)
        #action = robot.go_to_object(cube, distance_mm(30.0))
        #action.wait_for_completed()
        action = robot.dock_with_cube(cube, approach_angle=cozmo.util.degrees(0), num_retries=2)
        action.wait_for_completed()
        print("result:", action.result)

    else:
        robot.play_anim_trigger(cozmo.anim.Triggers.MajorFail)
        print("Failed")



#def main(robot: cozmo.robot.Robot):


cozmo.run_program(go_to_object_test)