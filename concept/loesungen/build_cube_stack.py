#!/usr/bin/env python3

import cozmo
import asyncio
from cozmo.util import degrees, distance_mm, speed_mmps

def sing_song(robot: cozmo.robot.Robot):
    # A list named notes, which contains the notes for the song.
    notes = [
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter)
    ]

    # Play the song as many times as the number at loop_count.
    # wait_for_completed()  => Wait until the song is over, continue with other behaviours afterwards.
    robot.play_song(notes, loop_count=1).wait_for_completed()

def find_cube_and_take_action(robot: cozmo.robot.Robot, color_of_cube, task_id):
    """ Looks around and tries to find one of Cozmo's cubes.
    If task_id equals 0, Cozmo will dock to the found cube. Any other number will make Cozmo to approach the found cube.
    """
    robot.set_head_angle(degrees(-5.0)).wait_for_completed() # Lower Cozmo's head by 5 radians.
    look_around = robot.start_behavior(cozmo.behavior.BehaviorTypes.LookAroundInPlace) # Look around to find cube.

    cube = None
    amount = 0

    try:
        # Try to find cube. If no cube is found afer 60 seconds it will stop.
        cube = robot.world.wait_for_observed_light_cube(timeout=60)
        print("Found cube", cube)

    except asyncio.TimeoutError:
        # Will be only executed if time is over.
        print("Didn't find a cube :-(")

    finally:
        # Stop looking around.
        look_around.stop()

    if cube:
        # If cube was found
        cube.set_lights(color_of_cube)
        if task_id == 0: # Check number of task_id to choose the right behaviour.
            action = robot.dock_with_cube(cube, approach_angle=cozmo.util.degrees(90), num_retries=2) # Dock to cube by an agngle of 90 degrees.
            amount = 2 # To raise lift later.
        else:
            action = robot.go_to_object(cube, distance_mm(40.0)) # Approach cube until it is only 40 mm left.
            amount = - 2 # To lower lift later.
        action.wait_for_completed()
        print("result:", action.result)


    robot.move_lift(amount) # Raise or lower lift with respect to amount.

def main(robot):
    robot.say_text("Hello, my name is Cozmo and I am going to pile up cubes.").wait_for_completed() # Cozmo says the text surrounded by "".
    find_cube_and_take_action(robot, cozmo.lights.blue_light, 0) # Dock to cube. Hands colour blue and action dock to cube over.
    find_cube_and_take_action(robot, cozmo.lights.green_light, 1) # Drive to other cube. Hands colour green and action approach cube over.
    robot.drive_straight(distance_mm(-100), speed_mmps(50)).wait_for_completed()  # Drive 100 mm backwards.
    sing_song(robot)

# Call of the programme
cozmo.run_program(main)