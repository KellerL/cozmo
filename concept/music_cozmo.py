import cozmo


def cozmo_program(robot: cozmo.robot.Robot):

    # Cozmo sagt den Text in Anfuehrungszeichen
    robot.say_text("Hallo, ich heisse Cozmo und summe euch mal etwas vor").wait_for_completed()

    # Legt eine Liste an, die die Noten fuer das Lied enthalten
    notes = [
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter)
    ]

    # Spielt das Lied so oft wie die Zahl die bei loop_count steht
    robot.play_song(notes, loop_count=1).wait_for_completed()


cozmo.run_program(cozmo_program)