#!/usr/bin/env python3

import cozmo
import asyncio
from cozmo.util import degrees, distance_mm, speed_mmps

def spiele_Lied(robot: cozmo.robot.Robot):
    # Eine Liste namens notes, die die Noten fuer das Lied enthalten wird angelegt.
    # TODO: Passe die Noten an das neue Lied an
    notes = [
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.E2, cozmo.song.NoteDurations.Half),
        cozmo.song.SongNote(cozmo.song.NoteTypes.A2_Sharp, cozmo.song.NoteDurations.Quarter),
        cozmo.song.SongNote(cozmo.song.NoteTypes.G2, cozmo.song.NoteDurations.ThreeQuarter)
    ]

    # Spielt das Lied so oft wie die Zahl die bei loop_count steht.
    # wait_for_completed()  => Wartet bis das Lied vorbei ist, erst dannch werden weitere Dinge ausgefuehrt.
    robot.play_song(notes, loop_count=1).wait_for_completed()


def suche_wuerfel_und_tu_aktion(robot: cozmo.robot.Robot, color_of_cube, task_id):
    """ Cozmo schaut in der Gegend herum um einen seiner Wuerfel zu entdecken.
    Wenn task_id gleich 0 ist, wird Cozmo an den gefundenen Wuerfel andocken. Bei allen anderen Zahlen wird Cozmo auf den Wuerfel zu gehen.
    """
    robot.set_head_angle(degrees(-5.0)).wait_for_completed() # Senke Cozmos Kopf 5 radians.
    look_around = robot.start_behavior(cozmo.behavior.BehaviorTypes.LookAroundInPlace) # Schaut sich um, um den Wuerfel zu finden.

    wuerfel = None
    wert = 0

    try:
        # Versuche Wuerfel zu finden. Wenn nach 60 Sekunden keienr gefunden wird, wird abgebrochen.
        wuerfel = robot.world.wait_for_observed_light_cube(timeout=60)
        print("Wuerfel gefunden", wuerfel)

    except asyncio.TimeoutError:
        # Wird nur ausgefuehrt wenn die Zeit vorbei ist.
        print("Wuerfel nicht gefunden :-(")

    finally:
        # Umherschauen beenden.
        look_around.stop()

    if wuerfel:
        # Wenn Wuerfel gefunden wurde.
        wuerfel.set_lights(color_of_cube)
        if task_id == 0: # Pruefe den Wert von task_id um das richtige Verhalten auszuwaehlen.
            action = robot.dock_with_cube(wuerfel, approach_angle=cozmo.util.degrees(90), num_retries=2) # An Wuerfel andocken im 90 Grad Winkel.
            wert = 2 # Um spaeter den Hebearm zu heben.
        else:
            action = robot.go_to_object(wuerfel, distance_mm(40.0)) # Approach cube until it is only 40 mm left.
            wert = - 2 # Um spaeter den Hebefarm zu senken.
        action.wait_for_completed()
        print("result:", action.result)

    robot.move_lift(wert) # Hebe oder Senke den Hebearm in Bezug auf wert.

def main(robot):
    # Hier startet das Programm
    suche_wuerfel_und_tu_aktion(robot, cozmo.lights.blue_light, 0) # An Wuerfel andocken. Farbe blau und Aktion an Wuerfel andocken wird mitgegeben.
    suche_wuerfel_und_tu_aktion(robot, cozmo.lights.green_light, 1) # Zu Wuerfel fahren. Farbe gruen und Aktion an Wuerfel annaehern wird mitgegeben.
    robot.drive_straight(distance_mm(-100), speed_mmps(
    50)).wait_for_completed()  # 100 mm rueckwarts fahren.
    spiele_Lied(robot)


# Aufruf des Programms
cozmo.run_program(main)